# site

Site web de l'association

Le script export.py génère un dossier build statique à déployer sur un serveur web.

## Tester en local

Créer un environnement python virtuel :
`python -m venv. venv`

Activer l'environnement virtuel :
`source .venv/bin/activate`

Check qu'on utilise bien l'environnement virtuel :
`which python`

Installer les dépendances :
`pip install -r requirements.txt`

Générer le site HTML via le script python :
`python export.py`

Autres arguments :
```
python export.py --help
usage: export.py [-h] [-o OUTPUT] [-f {gzip,folder,dev}] [-q]

Build website

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output folder or file
  -f {gzip,folder,dev}, --format {gzip,folder,dev}
                        Format output (either gzip, dev or folder)
  -q, --quiet
```

Déployer le serveur web en local :
`python -m http.server -d build`
